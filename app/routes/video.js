'use strict';

define([
	'app/controllers/videoController'
],
function(VideoController, auth){
	return function(app){
		var videoCtlr = VideoController(app);

		app.route('/user/:userName/video/:collectionName/:uuid').get(videoCtlr.onVideoRequested );
		return app;
	}
});