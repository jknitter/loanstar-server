'use strict';

define([
	'app/controllers/settingsController',
	'app/controllers/user/userAuthorizationController'
],
function(settingsCtlr, AuthCtlr){
	var checkLogin = AuthCtlr.checkLogin;

	return function(app){
		app.route('/api/settings')
		.get( checkLogin, settingsCtlr.getUserSettings )
		.patch( checkLogin, settingsCtlr.patchUserSetting );

		// Content Collections //
		app.route('/api/settings/collections/:name')
		// delete collection
		.delete( checkLogin, settingsCtlr.onDeleteContentCollection.bind(settingsCtlr, app) )
		// add new collection
		.put( checkLogin, settingsCtlr.onAddContentCollection.bind(settingsCtlr, app) )

		// Collection Paths //
		app.route('/api/settings/collections/:name/path')
		// add new pathToContent
		.post(checkLogin, settingsCtlr.onAddPathToContent.bind(settingsCtlr, app) )
			
		app.route('/api/settings/collections/:name/path/:uuid')
		// edit new pathToContent
		.put(checkLogin, settingsCtlr.onEditPathToContent.bind(settingsCtlr, app) )
		// delete path of a contentCollection
		.delete( checkLogin, settingsCtlr.onDeletePathToContent.bind(settingsCtlr, app) );
		
		return app;
	}
});