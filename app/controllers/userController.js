'use strict';

define([
	'underscore',

	'../../public/enums/socketMessageEnum',

	'./user/userAuthenticationController',
	'./user/userAuthorizationController',
	'./user/userPasswordController',
	'./user/userProfileController'
],
function( _, smEnum, userAuthenticationController, userAuthorizationController, userPasswordController, userProfileController){
	return _.extend(
		userAuthenticationController,
		userAuthorizationController,
		userPasswordController,
		userProfileController,
		{
			getNeighbors: function(app, req, res){
				var list = [];
				var pending = _.size(app.clients);
				
				//return if none
				if(pending === 0){
					return res.status(200).json(list);
				}

				_.each(app.clients, function(client, key){
					var socket = client.socket;
					
					socket.emit(smEnum.Neighbors_GetUserData, {}, function(user){
						list.push(user);			

						if(--pending === 0){
							return res.status(200).json(list);
						}
					});
				});
			}
		}
	);
});

	