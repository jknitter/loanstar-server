'use strict';

define([
	'underscore', 
	'../../public/enums/searchTypeEnum',
	'../../public/enums/socketMessageEnum'
],
function( _, SearchTypeEnum, smEnum){
	return {
		getSearchResults: function(app, req, res){
			var channel;
			switch(req.params.type){
				case SearchTypeEnum.Files:
					channel = smEnum.Search_GetMatchingFiles;
					break;
				case SearchTypeEnum.Dirs:
					//channel = smEnum.Search_GetMatchingDirs;
					return res.status(200).json([]);
					break;
				case SearchTypeEnum.Collections:
					channel = smEnum.Search_GetMatchingCollections;
					break;
				case SearchTypeEnum.Tags:
					return res.status(200).json([]);
					//channel = smEnum.Search_GetMatchingTags;
					break;
			};

			var fileList = [];
			var pending = _.size(app.clients);

			// will there ever be no one logged in?
			if(pending === 0) return res.status(200).json(fileList);
//TODO: what happens when one or all time out?
			_.each(app.clients, function(client, key){
				var socket = client.socket;
				socket.emit(channel, {term: req.params.term}, function(list){
					fileList = fileList.concat(list);			

					if(--pending === 0){
						return res.status(200).json(fileList);
					}
				});
			});
		}
	}
});