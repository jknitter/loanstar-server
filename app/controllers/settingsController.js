'use strict';

define([
	'underscore', 'config', 'async', 'fs', 'path', 'mongoose',
	'utils/js/namespace', 'utils/js/crypto',
	'public/controllers/errorsController',
	'app/controllers/responseController',

	'public/enums/socketMessageEnum'
],
function( _ ,config, async, fs, path, mongoose, nsUtil, cryptoUtil, errorsCtlr, responseCtlr, smEnum){
	var UserModel = mongoose.model('User');

	return {
		getUserSettings: function(req, res){
			UserModel.findOne({ userName: req.user.userName }, function(err, data){
				if(err){
					return res.status(400).json( responseCtlr.createErrorResponse(errorsCtlr.getValidation(err)) );
				}

				var dataObj = data.$toObject();
				var returnData = dataObj.settings;
				returnData.password = cryptoUtil.decrypt(dataObj.password);

				return res.status(200).json(returnData);
			});
		},

		patchContentCollections: function(app, req, res, actionEnum, emitConfig){
			var userName = req.user.userName;
			// patchUserSetting needs a req.body
			req.body = { contentCollections : req.user.settings.contentCollections };

			// TODO: might be better to run in a series reather than waterfall
			async.waterfall([
				// udate the db
				this.patchUserSetting.bind(this, req, res),
				// if that works, update the user's content/file list on their client-server
				function(settings, callback){
					emitConfig.settings = settings;
					var user = app.clients[userName];
					// dont try to update a client-server that isnt connected
					if(user){
						user.socket.emit(actionEnum, emitConfig);
					}
				}	
			], 
			function(err, results){
			//	if(err){} 
			//console.log('results = ' + results)
			});
		},

		patchUserSetting: function(req, res, callback){
			var config = req.body;
			//TODO: design a better workflow for this
			if(config.password){
				config = { password: cryptoUtil.encrypt(req.body.password) };
			}
			else if(config.port){
				config = { 'settings.port': config.port };
			} 
			else if(config.contentCollections){
				config = { 'settings.contentCollections': config.contentCollections };
			}
			
			// save to server database 
			UserModel.findOneAndUpdate({ userName: req.user.userName }, config, {new: true}, function(err, data){
		 		if(err){
					res.status(400).json( responseCtlr.createErrorResponse( errorsCtlr.getValidation(err)) );
					// for waterfall
					if(callback) callback(err);
					return;
				}

				var returnData = data.$toObject();	
				res.status(200).json(returnData.settings);

				// for waterfall
				if(callback) callback(null, returnData.settings);
				
				return;
			});
		},

		onAddContentCollection: function(app, req, res){
			var collectionName = req.params.name;
			// add collection
			req.user.settings.contentCollections.push({ name: collectionName, paths: [] });
			// save to db
			this.patchContentCollections(app, req, res, smEnum.Settings_AddContentCollection, { name: collectionName });			
		},

		onAddPathToContent: function(app, req, res){
			var collectionName = req.params.name;
			// get collection
			var collection = _.findWhere(req.user.settings.contentCollections, { name: collectionName });
			collection.paths.push(req.body);

			// save to db
			this.patchContentCollections(app, req, res, smEnum.Settings_AddPathToContent, { name: collectionName, path: req.body.path });	
		},

		onDeleteContentCollection: function(app, req, res){
			var collectionName = req.params.name;
			var settings = req.user.settings;
			
			// remove collection
			var collection = _.findWhere( settings.contentCollections, { name: collectionName });
			var index = settings.contentCollections.indexOf(collection);
			settings.contentCollections.splice(index, 1);
			
			// save to db
			this.patchContentCollections(app, req, res, smEnum.Settings_DeleteContentCollection, { name: collectionName });
		},

		onDeletePathToContent: function(app, req, res){
			var collectionName = req.params.name;
			var pathId = req.params.uuid;
			var settings = req.user.settings;
			
			// collection contining path
			var collection = _.findWhere( settings.contentCollections, { name: collectionName });
			// path to delete
			var path = _.findWhere(collection.paths, { id: pathId });
			
			var index = collection.paths.indexOf(path);
			if(index > -1)
				collection.paths.splice(index, 1);

			// patchUserSetting needs a req.body
			req.body = { contentCollections : settings.contentCollections };

			// save to db
			this.patchContentCollections(app, req, res, smEnum.Settings_DeletePathToContent, { name: collectionName, path: path.path });
		},

		onEditPathToContent: function(app, req, res){
			// body comes in with id of path to remove and path to add
			var collectionName = req.params.name;
			// get collection
			var collection = _.findWhere(req.user.settings.contentCollections, { name: collectionName });
			var pathToContent = _.findWhere(collection.paths, { id: req.body._id });
			var oldPath = pathToContent.path;
			var newPath = req.body.path;

			var index = collection.paths.indexOf(pathToContent);
			if(index > -1)
				collection.paths.splice(index, 1, { path: newPath });

			// save to db
			this.patchContentCollections(app, req, res, smEnum.Settings_EditPathToContent, { name: collectionName, path: newPath, oldPath: oldPath });	
		},

	}
});