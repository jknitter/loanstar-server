'use strict';

define([
	'underscore', 
	'../../public/enums/socketMessageEnum',
	'app/controllers/user/csAuthController'
],
function( _ , smEnum, csAuthCtlr){
	return function(app){
		var clients = app.clients;
		var contentCollections = app.contentCollections;
		
		function onClientInitComplete(data){
			console.log(data.userName + ' @ ' + this.handshake.address + ' intitialized');
			// once the client server sends the file list
			if(!clients[data.userName]){
				clients[data.userName] = {};
				clients[data.userName].socket = this;
				// for ref on disconnect
				clients[data.userName].socket.userName = data.userName;
			}
		}
		
		function onClientConnection(socket) {
			// client socket is passed in, set up what it can handle
			console.log(socket.handshake.address + ' connected at ' + new Date().toUTCString());
			// server sends this back when client-server connection is complete
			// TODO: change to use acknowldge callback
			socket.on( smEnum.clientInitComplete, onClientInitComplete.bind(socket) );
			socket.on( smEnum.disconnect, function(){ userDisconnected(socket);	});
			socket.emit( smEnum.connectionComplete, {id: socket.id} );

			socket.on( smEnum.User_csLogin, csAuthCtlr.csLogin.bind(csAuthCtlr, app) );
		}

		function userDisconnected(socket) {
			console.log(socket.userName + ' userDisconnected @ ' + socket.handshake.address + ' disconnected -' + new Date().toUTCString());
	 		delete app.clients[socket.userName];
		}		

		return {
			init: function(socket){
				// set up server handlers
				socket.on( smEnum.connection, onClientConnection );
			}
		};
	}
});