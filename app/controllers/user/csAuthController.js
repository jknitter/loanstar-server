'use strict';

define([
	'underscore', 'mongoose', 
	'app/controllers/strategies/local', 
	'utils/js/crypto'
],
function( _, mongoose, doAuthFn, CryptoUtils){
	var UserModel = mongoose.model('User');

	return { 
		csLogin: function(app, config, callback){
			// if this user already has a client server logged in
			if(app.clients[config.userName]){
				callback('User already has a client-server logged in.');
			}

			try {
				config.password = CryptoUtils.decrypt(config.password);
			}
			catch(err){
				return callback('Error decrypting in cs login');
			}

			doAuthFn(config.userName, config.password, function(err, user, info) {
				if(err || !user){
					return callback(info.message);
				}
				else {
					// Remove sensitive data before login
					user.password = undefined;
					user.salt = undefined;

					user = user.toObject();
				
					return callback(null, user);
				}
			});
		}
	}
});