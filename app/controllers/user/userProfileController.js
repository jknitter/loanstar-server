'use strict';

define([
	'underscore', 'mongoose', 'passport',
	'public/controllers/errorsController', 'app/controllers/responseController',
	'public/enums/socketMessageEnum'
],
function( _ , mongoose, passport, errorsCtlr, responseCtlr, smEnum){
	return {
		update: function(req, res) {
			// Init Variables
			var user = req.user;
			var message = null;

			// For security measurement we remove the roles from the req.body object
			delete req.body.roles;

			if (user) {
				// Merge existing user
				user = _.extend(user, req.body);
				user.updated = Date.now();
				user.displayName = user.firstName + ' ' + user.lastName;

				user.save(function(err) {
					if (err) {
						return res.status(400).json( responseCtlr.createErrorResponse(errorsCtlr.getValidation(err)) );
					} else {
						req.login(user, function(err) {
							if (err) {
								return res.status(400).json( responseCtlr.createErrorResponse(errorsCtlr.getValidation(err)) );
							} else {
								return res.jsonp(user);
							}
						});
					}
				});
			} else {
				return res.status(400).json( responseCtlr.createErrorResponse(errorsCtlr.getValidation('User is not signed in')) );
			}
		},

		me: function(req, res) {
			res.jsonp(req.user || {});
		},

		getUserCollections: function(app, req, res){
			var client = app.clients[req.params.userName];
			if(!client){	
				// use "your" vs userName
				var possessiveRef = (req.user.userName === req.params.userName)? 'Your': req.params.userName +'\'s';
				return res.status(400).json(  
					responseCtlr.createErrorResponse(errorsCtlr.getValidation('There are no collections available. ' + possessiveRef +' client-server is not connected.'))
				);
			}
			else {
				client.socket.emit(smEnum.User_GetUserCollections, {name: req.params.collectionName}, function(list){
					return res.json(list || []);
				});	
			}
		}
	}
});