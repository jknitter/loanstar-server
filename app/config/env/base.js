'use strict';

define([
	'underscore',
	'devConfig' 
	// environment config only needs "development" atm
	// this way gulp can overwrite this file
],
function(_, envConfig){
	return  envConfig
});