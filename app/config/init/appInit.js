'use strict';

// loaded first in index.js
define([
	'globber', 'path', 'config', 'expressInit',
	'utils/file/config'
],
function( globber, path, config, app, configUtils ){
	app.clients = {};
	// app.config
	app.locals.description = app.config.description;
	//app.locals.facebookAppId = app.config.facebook.clientID;
	app.locals.keywords = app.config.keywords;
	app.locals.title = app.config.title;
	
	// files the layout html should include
	app.locals.cssFiles = configUtils.getCSSAssets( app.config.assets.css );
	app.locals.jsFiles = configUtils.getJavaScriptAssets( app.config.assets.js );

	return app;
});