'use strict';

var requirejs = require('requirejs');
requirejs.config({
	baseUrl: '.',
	nodeRequire: require,
	waitSeconds: 0,

	// please keep in abc order
	paths: {
		'appInit': 'app/config/init/appInit',
		'expressInit': 'app/config/init/expressInit',
		
		'config': 'app/config/env/base',
		'devConfig': 'app/config/env/development',

		'globber': 'utils/file/globber'
	}
});

require.extensions['.server.controller.js'] = require.extensions['.js'];
require.extensions['.server.model.js'] = require.extensions['.js'];
require.extensions['.server.routes.js'] = require.extensions['.js'];

module.exports = requirejs;
