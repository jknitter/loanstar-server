'use strict';

define([
	'knockout',
	'knockback',
	
	'modules/core/viewmodels/contentCollectionViewModel',
	'modules/core/collections/contentCollection',

	'utils/js/namespace',	

	'modules/core/bindings/valid',
	
	'template!modules/profile/templates/contentItem.html',
	'components/fileOverlay/binding'
],
function(ko, kb, ContentCollectionViewModel, ContentCollection, nsUtil){
	var openMovieAPIKey = 'a8ba9fca24a102edfa9d399d8d27c962';
	var tmdbSearchUrlBase = 'http://api.themoviedb.org/3/search/movie?api_key='+ openMovieAPIKey +'&query=';

	return kb.ViewModel.extend({
		constructor: function(userName) {
			this.userNameInPath = userName;	
			this.isLoading = ko.observable(true);
			this.navCollapsed = ko.observable(false);
			this.selectedFile = ko.observable();
			this.selectedNavItem = ko.observable();
			this.showFileOverlay = ko.observable(false);
			this.validations = ko.observable({});
			this.isMe = ko.observable(this.userNameInPath === App.User.get('userName'));
	
			this.hasError = ko.computed(function(){
				return _.size(this.validations()) > 0;
			}, this);

			this.contentCollections = kb.collectionObservable(new ContentCollection(), {view_model: ContentCollectionViewModel});
			this.contentCollections.collection().fetchByUser( this.userNameInPath ).always(function(model, resp){
				this.isLoading(false);
			}.bind(this));

			this.selectedContentCollection = ko.computed(function(){
				var selectedNav = this.selectedNavItem();
				if(selectedNav)
					return _.find(this.contentCollections(), function(cc){ return cc.name() === selectedNav.name.peek(); });
				else
					//set it to the first one
					this.selectedNavItem(this.contentCollections()[0]);
			}, this);

			this.showNoCollections = ko.computed(function(){
				return !this.hasError() && !this.isLoading() && this.contentCollections().length === 0;
			}, this);	

			this.showNoContent = ko.computed(function(){
				return 	(!this.hasError() && !this.isLoading() && 
						this.contentCollections().length > 0 && 
						this.selectedContentCollection() && this.selectedContentCollection().contentItems().length === 0);
			}, this);

			this.showContent = ko.computed(function(){
				return (!this.hasError() && !this.isLoading() && this.contentCollections().length > 0 && 
						this.selectedContentCollection() && this.selectedContentCollection().contentItems().length > 0);
			}, this);	
		},

		onDirClick: function(vm, e){
			e.stopPropagation();
			vm.collapsed( !vm.collapsed.peek() );
		},

		onFileClick: function(vm, e){
			e.stopPropagation();
			// closing open file
			if(this.selectedFile.peek() !== vm)
				this.selectedFile(vm);

			this.showFileOverlay(true);
		},

		onNavCollapseClick: function(){
			this.navCollapsed(!this.navCollapsed.peek());
			return false;
		},

		onNavItemClick: function(vm, e){
			if(this.selectedNavItem() != vm){
				this.selectedFile(null);
			}
			
			this.selectedNavItem(vm);
		}
	});
});