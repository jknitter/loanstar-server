'use strict';

define([
	'backbone', 'bbr',
	'modules/core/models/contentItemModel',
	'modules/core/collections/contentItemCollection'
],
// everything about a single "content collection"
function(Backbone, bbr, ContentItemModel, ContentItemCollection){
	var ContentCollectionModel = Backbone.RelationalModel.extend({ 
		defaults: {
			contentItems: [], name: ''
		},

		relations:[{
			type: Backbone.HasMany,
			key: 'contentItems',
			relatedModel: ContentItemModel,
			collectionType: ContentItemCollection
		}]
	});

	return App.Utils.ns(App, 'Core.Models.ContentCollectionModel', ContentCollectionModel);
});