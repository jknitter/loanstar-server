'use strict';

define([
	'backbone', 'bbr', 'bbv',
	'modules/core/collections/contentItemCollection'
],
function(Backbone, bbr, bbv, ContentItemCollection){
	var ContentItemModel = Backbone.RelationalModel.extend({
		idAttribute: 'uuid',
		
		defaults: {
			isDir: false
		},
		
		// if this particular content item is a representation of a directory
		// we anything inside that directory is also treated as a ContentItemModel
		relations: [{
			type: Backbone.HasMany,
			key: 'contentItems',
			relatedModel: ContentItemModel,
			collectionType: ContentItemCollection
		}]
	});
	return App.Utils.ns(App, 'Core.Models.ContentItemModel', ContentItemModel);
});