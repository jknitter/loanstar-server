'use strict';

define([
	'backbone',
	'modules/core/models/contentCollectionModel'
],

//an array of contentItemCollection
function(Backbone, ContentCollectionModel){
	var ContentCollection = Backbone.Collection.extend({
		fetchByUser: function(userName){
			return this.fetch({url: '/api/user/'+ userName +'/collections'});
		},

		model: ContentCollectionModel
	});

	return App.Utils.ns(App, 'Core.Collections.ContentCollection', ContentCollection);
});