'use strict';

define([
	'backbone',
	'modules/core/models/contentItemModel'
],
// a collection of individual content item models, which includes the file path
function(Backbone, ContentItemModel){
	var ContentItemCollection = Backbone.Collection.extend({
		model: ContentItemModel
	});

	return App.Utils.ns(App, 'Core.Collections.ContentItemCollection', ContentItemCollection);
});