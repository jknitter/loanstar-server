'use strict';

define([
	'knockout',
	'knockback'
],
function(ko, kb){
	var ContentItemViewModel = kb.ViewModel.extend({
		constructor: function(model, options) {
			kb.ViewModel.prototype.constructor.call(this, model, [
				'collectionName', 'isDir', 'name', 'path', 'userName', 'uuid'
			]);

		 	this.contentItems = kb.collectionObservable(model.get('contentItems'), {view_model: ContentItemViewModel} )
		 	this.collapsed = ko.observable(true);
		 	this.isPlaying = ko.observable(false);
	 
		 	this.skipDisplay = ko.computed(function(){
		 		// if there is only one child in this dir and the child is a file
		 		return this.isDir.peek() && this.contentItems.peek().length === 1 && !this.contentItems.peek()[0].isDir.peek();
		 	}, this);

		 	this.showContentItems = ko.computed(function(){
		 		return this.contentItems().length > 0 && !this.collapsed();
		 	}, this);
		},

		getVideoUrl: function(vm){
			return '/user/' + this.userName.peek() + '/video/' + this.collectionName.peek() + '/' + this.uuid.peek();
		}
	});	

	return ContentItemViewModel;
});