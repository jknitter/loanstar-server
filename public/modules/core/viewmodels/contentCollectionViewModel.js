'use strict';

define([
	'knockout',
	'knockback',

	'modules/core/viewmodels/contentItemViewModel'
],
function(ko, kb, ContentItemViewModel){
	return kb.ViewModel.extend({
		constructor: function(model, options) {
			// when this is in a list like on search page, treat this like a dir
			this.isDir = ko.observable(true);
			this.collapsed = ko.observable(true);	
		 	
			this.model = ko.observable(model);
			this.name = kb.observable(model, 'name');
			this.contentItems = kb.collectionObservable(model.get('contentItems'), {view_model: ContentItemViewModel});		

			this.showContentItems = ko.computed(function(){
		 		return this.contentItems().length > 0 && !this.collapsed();
		 	}, this);
	    }
	});
});