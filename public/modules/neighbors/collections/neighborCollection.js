'use strict';

define([
	'backbone',
	'modules/neighbors/models/neighborModel'
],
function(Backbone, NeighborModel){
	var NeighborCollection = Backbone.Collection.extend({
		model: NeighborModel,
		url: function(){
			return '/api/neighbors';
		}
	});

	return App.Utils.ns(App, 'Settings.Collections.NeighborCollection', NeighborCollection);
});