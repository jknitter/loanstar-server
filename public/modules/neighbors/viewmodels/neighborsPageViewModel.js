'use strict';

define([
	'knockout',
	'knockback',

	'modules/neighbors/collections/neighborCollection',
	'modules/neighbors/viewmodels/neighborViewModel'

],
function(ko, kb, NeighborCollection, NeighborViewModel){
	return kb.ViewModel.extend({
		constructor: function(){
			this.neighbors = kb.collectionObservable(new NeighborCollection(), {view_model: NeighborViewModel});
			this.neighbors.collection().fetch().always(function(model, resp){
				//this.isLoading(false);
			}.bind(this));
		}
	});
});