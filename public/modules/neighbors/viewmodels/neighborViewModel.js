'use strict';

define([
	'knockout',
	'knockback',

	'modules/core/viewmodels/contentItemViewModel'
],
function(ko, kb, ContentItemViewModel){
 	return kb.ViewModel.extend({
		constructor: function(model, options) {
			this.model = ko.observable(model);
			this.userName = kb.observable(model, 'userName');
			this.collectionCount = kb.observable(model, 'collectionCount');
			this.fileCount = kb.observable(model, 'fileCount');	
	    }
	});
});