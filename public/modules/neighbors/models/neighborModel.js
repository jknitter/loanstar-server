'use strict';

define([
	'backbone'
],
function(Backbone){
	var NeighborModel = Backbone.RelationalModel.extend({
		idAttribute: 'userName',

		urlRoot: function(){
			return "/api/user";	
		}
	});

	return App.Utils.ns(App, 'Settings.Models.NeighborModel', NeighborModel);
});