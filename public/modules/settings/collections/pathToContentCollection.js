'use strict';

define([
	'backbone',
	'modules/settings/models/pathToContentModel'
],

//an array of contentItemCollection
function(Backbone, PathToContentModel){
	var PathToContentCollection = Backbone.Collection.extend({
		model: PathToContentModel
	});

	return App.Utils.ns(App, 'Settings.Collections.PathToContentCollection', PathToContentCollection);
});