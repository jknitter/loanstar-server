'use strict';

define([
	'backbone',
	'modules/settings/models/pathToContentCollectionModel'
],
//an array of pathToContentCollectionModel
function(Backbone, PathToContentCollectionModel){
	var ContentCollection = Backbone.Collection.extend({
		model: PathToContentCollectionModel
	});

	return App.Utils.ns(App, 'Settings.Collections.ContentCollection', ContentCollection);
});