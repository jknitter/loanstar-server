'use strict';

define([
	'backbone', 'bbr'
],
function(Backbone, bbr){
	var PathToContentModel = Backbone.RelationalModel.extend({ 
		idAttribute: '_id',

		urlRoot: function(){
			var parentCollection = this.get('collectionOfPath');
			return 'api/settings/collections/' + parentCollection.get('name') + '/path';
		},

		validation: {
			path: { required: true }
		}
	});

	return App.Utils.ns(App, 'Settings.Models.PathToContentModel', PathToContentModel);
});