'use strict';

define([
	'backbone', 'bbr',
	'modules/settings/models/pathToContentModel',
	'modules/settings/collections/pathToContentCollection'
],
// everything about a "content collection object"
function(Backbone, bbr, PathToContentModel, PathToContentCollection){
	// collection in name, since we use the parent object's properties as a collection
	var PathToContentCollectionModel = Backbone.RelationalModel.extend({ 
		idAttribute: 'name',

		urlRoot: function(){
			return 'api/settings/collections/';
		},

		defaults: {
			paths: []
		},

		relations:[{
			type: Backbone.HasMany,
			key: 'paths',
			relatedModel: PathToContentModel,
			collectionType: PathToContentCollection,
			reverseRelation: {
				key: 'collectionOfPath',
				includeInJSON: false
			}
		}]
	});

	return App.Utils.ns(App, 'Settings.Models.PathToContentCollectionModel', PathToContentCollectionModel);
});