'use strict';

define([
	'backbone', 'bbr', 'underscore',
	'modules/settings/models/pathToContentCollectionModel',
	'modules/settings/collections/contentCollection'
],
function(Backbone, bbr, _ , PathToContentCollectionModel, ContentCollection){
	var SettingsModel = Backbone.RelationalModel.extend({	
		url: function(){
			return "/api/settings";	
		},

		defaults: {
			port: '',
		},
	
		relations: [{
			type: Backbone.HasMany,
			key: 'contentCollections',
			relatedModel: PathToContentCollectionModel,
			collectionType: ContentCollection
		}],

		validation: {
			password: {
		        minLength: 8
		    },
		    port: {
		    	range: [1000, 65000]
		    }
		}
	});

	return App.Utils.ns(App, 'Settings.Models.SettingsModel', SettingsModel);
});