'use strict';

define([
	'knockout',
	'knockback',

	'modules/settings/viewmodels/pathToContentViewModel'
],
function(ko, kb, PathToContentViewModel){
	return kb.ViewModel.extend({
		constructor: function(model, options) {
			this.model = ko.observable(model);
			this.name = kb.observable(model, 'name');
			this.paths = kb.collectionObservable(model.get('paths'), {view_model: PathToContentViewModel});
	    }
	});
});