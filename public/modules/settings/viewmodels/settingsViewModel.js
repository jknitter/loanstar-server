'use strict';

define([
	'underscore',
	'knockout',
	'knockback',

	'modules/settings/models/pathToContentCollectionModel',
	'modules/settings/models/pathToContentModel',
	'modules/settings/viewmodels/contentCollectionViewModel',
	'controllers/errorsController',

	'template!modules/settings/templates/contentCollection.html',

	'components/editSaveInput/binding',
	'modules/core/bindings/valid'
],
function( _ , ko, kb, PathToContentCollectionModel, PathToContentModel, ContentCollectionVM, errCtlr){
	return kb.ViewModel.extend({
	    constructor: function(model, options) {
	    	this.originalUserSettings = kb.viewModel(model.clone());
	    	this.model = ko.observable(model);
	    	
	    	// Account Settings //
	    	this.userName = kb.observable(App.User, 'userName');
	    	this.password = kb.observable(model, 'password');
	    	this.port = kb.observable(model, 'port');
	    	// Content Collections DropDown 
	    	var contentCollections = model.get('contentCollections');
		    this.contentCollections = kb.collectionObservable(contentCollections, {view_model: ContentCollectionVM});
			
	      	this.showConfirmPassword = ko.observable(false);

	    	// Content Settings //
  			// Adding a new collection (name)
		    this.addingContentCollection = ko.observable(false);
		    this.addingContentCollectionName = ko.observable('');

			// Adding a file path to a colletion
			this.addingContentItem = ko.observable(false);
		    
		    this.selectedContentCollectionValue = ko.observable();
		    this.selectedContentCollection = ko.computed(function(){
		    	var value = this.selectedContentCollectionValue();
		    	if(value){
		    		this.onCancelAddPathToContentClick();
		    	}

				var toreturn = _.find(this.contentCollections(), function(cc){
					return cc.name.peek() === value;
				}, this);

				return toreturn;
		    }, this);

		    App.Events.on('editEnabled', function(eventOrigniator){
		    	if(eventOrigniator !== this) this.onCancelAddContentCollectionClick()
		    }.bind(this));
			
			if(this.contentCollections().length === 0){
				this.validations = ko.observable()
			    // turn on the Add first collection message when needed
			    this.validations(errCtlr.getValidation('Add a collection here!', 'contentCollections'));
			}
		},

		doPatch: function(property, val){
			var config = {};
			config[property] = val;

			return this.model().save(config, {
				patch: true,
				wait: true,
				success: function(newModel, json){
					// manually udpdate user since we use a cloned settings model for the settings page
					App.User.get('settings').set(json);

				}.bind(this),
				error: function(){
					debugger
				}
			});
		},

		cancelEditingPath: function(){
		 	this.addingContentItem(false);
		},

		// Content Settings //
		onAddContentCollectionClick: function(){
			this.validations({});
			
			this.onCancelAddPathToContentClick();

			this.selectedContentCollectionValue(null);
			this.addingContentCollection(true);

			App.Events.trigger('editEnabled', this);
 		},

		onAddPathToContentClick: function(){
			this.validations({});
			this.addingContentItem(true);
			
 			var newPathModel = this.selectedContentCollection().paths.collection().add({ path: '' });
			
			App.Events.trigger('editEnabled', newPathModel);
 		},
 		
 		onCancelAddContentCollectionClick: function(){
		 	this.validations({});
			
		 	this.addingContentCollectionName('');
		 	this.addingContentCollection(false);
		},

		onCancelAddPathToContentClick: function(){
		 	this.validations({});
			
			var curCollection = this.selectedContentCollection();
			// only remove the path if the newest path in the collection is unsaved
 			if(curCollection){
 				var last = curCollection.paths.collection().last();
 				if(last && last.isNew()){
 					curCollection.paths.collection().pop();
 					this.cancelEditingPath();
 				}
 			}
		},		

 		onRemoveCollectionClick: function(){
 			this.validations({});
			
 			var collectionName = this.selectedContentCollectionValue();
 			var collection = this.contentCollections.collection().findWhere({ name: collectionName });
 			
 			return collection.destroy();
 		},

 		onRemoveContentItemClick: function(vm, e){
 			this.validations({});
			this.onCancelAddPathToContentClick();

 			return vm.model().destroy({wait: true});
 		},

		onSaveNewContentCollectionClick: function(){
			this.validations({});
			
			var newCollectionName = this.addingContentCollectionName.peek();
			if(!_.isEmpty(newCollectionName)){
	 			var collectionList = this.contentCollections.collection();
	 			var newCollectionObj = new PathToContentCollectionModel({ "name": newCollectionName});
	 			// save the new collection to the db
	 			newCollectionObj.save()
	 			.then(function(){
	 				collectionList.add(newCollectionObj);	
					
					this.onCancelAddContentCollectionClick();

	 				this.selectedContentCollectionValue(newCollectionObj.get('name'));
	 			}.bind(this))
	 			.fail(function(){
	 				debugger
	 			});
			}
			else {
				this.validations( errCtlr.getValidation('Can not be empty', 'contentCollections') )
			}
 		},

		onSaveEditPathToContentClick: function(key, val, origVal){
			var curCollection = this.selectedContentCollection();
			// only remove the path if the newest path in the collection is unsaved
 			if(curCollection){
 				var pathToContentModel = curCollection.paths.collection().findWhere({ path: val });
 				if(pathToContentModel){
 		 			return pathToContentModel.save({ path: val });
 		 		}
 		 	}
		},

 		onSaveNewPathToContentClick: function(key, val){
 			var curCollection = this.selectedContentCollection();
			// only remove the path if the newest path in the collection is unsaved
 			if(curCollection){
 				var pathToContentModel = curCollection.paths.collection().last();
 				if(pathToContentModel){
 		 			return pathToContentModel.save()
		 				.then(function(settings){
		 					// currently since we just blindly return contentCollection after settings patch
		 					// we have to manually update the model the server just saved
							var updatedCollection = _.findWhere( settings.contentCollections, { name: curCollection.name() });
		 					var updatedPath = _.findWhere( updatedCollection.paths, { path: pathToContentModel.get('path')});
		 					var outOfDataPath = curCollection.paths.collection().findWhere({ path: pathToContentModel.get('path') });

		 					// remove the add since this editSaveInput
		 					curCollection.paths.collection().remove(outOfDataPath);
							curCollection.paths.collection().add(updatedPath);

							this.cancelEditingPath();
			 			}.bind(this))
			 			.fail(function(){
			 				console.log('fail')
			 			});
		 		}
		 		else	
		 			return new Promise().reject('model not found');
		 	}
		 	else
		 		// should never happen, given ui workflows
		 		return new Promise().reject('no selected collection');


 		},

 		onSaveEditPort: function(){
 			this.validations( this.model().validate() );

 			if(!this.validations['port'])
 				return this.doPatch('port', this.port());
 			else {
 				// TODO: return deferred
 			}		
 		}
	});
});