'use strict';

define([
	'jquery',
	'knockout',
	'knockback',
    'components/videoPlayer/binding',
    'components/fileInfo/binding'
],
function($, ko, kb){
    var OverlayStates = {
        Play: 'file-player',
        Info: 'file-info',
        Tags: 'file-tags'
    };

    var FileOverlayViewModel = kb.ViewModel.extend({
        constructor: function(params){
            this.file = params.file.peek();
            this.name = this.file.name.peek();
            this.show = params.show;

            this.OverlayStates = OverlayStates;
            this.overlayState = ko.observable(OverlayStates.Play);
        },
        
        setOverlayState: function(state){
            if(state && state !== this.overlayState.peek()){
                this.overlayState(state);
            }
        },

        onCloseClick: function(){
        	this.show(false);
        }
    });

    App.Utils.ns(App, 'Core.ViewModels.FileOverlayViewModel', FileOverlayViewModel);
    return FileOverlayViewModel;
});