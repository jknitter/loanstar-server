'use strict';

define([
	'jquery',
	'underscore',
	'knockout',
	'knockback'
],
function($, _ , ko, kb){
	var FileInfoViewModel = kb.ViewModel.extend({
		constructor: function(options){
	    	if(!options.file) throw "FileInfoViewModel component was constructed without a file option";
			
			_.extend(this, options.file);

//			var id = this.getIMDBId();
	    },

	    getIMDBId: function(){
	    	debugger
			return this.getIMDBInfo(this.name.peek()).then(
				function(resp){    		
    				debugger;
    				//this.imdbId
    			}.bind(this)
	    	);
	    },

	    getIMDBInfo: function(name){
	    	var urlBase = 'http://www.imdb.com/xml/find?json=1&nr=1&tt=on&q=';

            return $.ajax({
            	dataType: 'json',
            	contentType: 'application/json',
            	url: urlBase +  name,
            	success:function(resp){
            		debugger
            	}, error: function(){
            		debugger
            	}
            }).then(function(){
            	debugger
            });

	    }
	});

	return FileInfoViewModel;
});