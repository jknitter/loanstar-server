'use strict';

define([
	'knockout', 

	'components/fileInfo/viewModel',
	'text!components/fileInfo/template.html' 
],
function(ko, vm, template){
	ko.components.register('file-info', {
		viewModel: {
			createViewModel:function(params, config){
				var options = _.extend(params, { bindingEl: config.element });
				return new vm(options);
			}
		},
		template: $(template).html()
	});
});