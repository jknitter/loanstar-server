'use strict';

define(function(){
 	return  {
		// Reserved
		connection: 			'connection',
		disconnect: 			'disconnect',
		error: 					'error',	
		
		// Custom
		connectionComplete: 	'CC',
		fileStreamRequest:		'FSRq',
		fileStreamResponse:		'FSRs',
		clientInitComplete: 	'CIC',
		peerDisconnected: 		'PD',
		requestedFileLocked:	'RFL',
		sendUsers: 				'SU',

		// API
		Neighbors_GetUserData: 'Neighbors_GetUserData',

		Search_GetMatchingCollections: 'Search_GetMatchingCollections',
		Search_GetMatchingFiles: 'Search_GetMatchingFiles',
		Search_GetMatchingDirs: 'Search_GetMatchingDirs',
		Search_GetMatchingTags: 'Search_GetMatchingTags',

		Settings_AddContentCollection: 'Settings_AddContentCollection',
		Settings_AddPathToContent: 'Settings_AddPathToContent',
		Settings_DeleteContentCollection: 'Settings_DeleteContentCollection',
		Settings_DeletePathToContent: 'Settings_DeletePathToContent',
		Settings_EditPathToContent: 'Settings_EditPathToContent',

		User_GetUserCollections: 'User_GetUserCollections',
		User_csLogin: 'User_csLogin'
	}
});