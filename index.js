'use strict';
	
var requirejs = require('./app/config/init/requireInit');
requirejs([
	'appInit',
	'http',
	'socket.io'
],
function(app, http, io){
	var port = process.env.PORT || app.config.port || 3001;
	var server = http.createServer(app);
  	var socket = io.listen(server);
  	
  	requirejs([
		'app/controllers/socketController'
	],
	function(SocketController){
		app.socketController = SocketController(app);
		app.socketController.init(socket);

		server.listen(port, function() { 
			console.log('Server started on port %s', port);
		});
	});
});