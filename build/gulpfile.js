var exec = require('child_process').exec,
	del = require('del'),
	fs = require('fs'),
    eol = require('os').EOL,
	gulp = require('gulp'),
   
    less = require('gulp-less'),
    rename = require('gulp-rename'),
	//minifyCSS = require('gulp-minify-css'),
    uglify = require('gulp-uglify');

var basePath = '../';
var prodRoot = basePath+'dist/public';

//**** main tasks to bundle the app ****//
gulp.task('prod', ['prod-copy', 'prod-rjs', 'prod-less']);

gulp.task('prod-copy', function(){
	// copy all under app, keep data folder too, but no data, config too
	gulp.src(['app/**/*', '!app/data/**', '!app/config/env/**', '!app/views/**'], { cwd: basePath })
	.pipe(uglify())
	//.on('error', function(err){ console.log(err); })
	.pipe(gulp.dest(basePath+'dist/app'));

	gulp.src(['app/views/**'], { cwd: basePath })
	.pipe(gulp.dest(basePath+'dist/app/views'));
	
	gulp.src(['app/config/env/production.js'], { cwd: basePath })
	.pipe(uglify())
	.pipe(rename('base.js'))
	.pipe(gulp.dest(basePath+'dist/app/config/env/'));

	// copy all package files '.bowerrc, bower.json, package.json, server.js
	gulp.src(['.bowerrc', 'bower.json', 'package.json', 'index.js'], { cwd: basePath })
	.pipe(gulp.dest(basePath+'dist'));

	gulp.src(['utils/**/*'], { cwd: basePath })
	.pipe(uglify())
	.pipe(gulp.dest(basePath+'dist/utils'));
});

gulp.task('prod-rjs', function(){
	console.log('Starting rjs, this can take a minute or two.')
	exec("node r.js -o rjs.build.js", function(err, stdout, stderr) {
		if(err) console.log(err);

		//create the data folder
		try { fs.mkdirSync('../dist/app/data'); }
		catch(e) { if( e.code != 'EEXIST' ) throw e; }

		// delete less folder
		del(prodRoot + '/less', {force: true});
		console.log("delete dist/less");

		// remove copied lib folder, when we npm install the build let bower take over
		del(prodRoot + '/lib', {force: true});
		console.log("delete dist/lib");

		console.log("rjs complete");	
   	}); 
});

gulp.task('prod-less', function(){
	gulp.src(['public/less/app.less'], { cwd: basePath })
		.pipe(less())
		.pipe(gulp.dest(basePath+'dist/public/css'));
	console.log("prod-less complete");
});


//**** end main tasks to bundle the app ****//


gulp.task('less', function() {
    return gulp.src(['public/less/app.less'], { cwd: basePath })
                .pipe(less())
                .pipe(gulp.dest(basePath+'public/css'));
});

gulp.task('wless', ['less'], function() {
    gulp.watch(basePath+'public/less/**', ['less']);
});



