({
    appDir: "../public",
    baseUrl: ".",
    dir: "../dist/public",
    mainConfigFile: '../public/config/requireInit.js',
    optimize: "uglify2"
})